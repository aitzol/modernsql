package com.buntplanet.formacion.modernsql;

import javax.sql.DataSource;
import java.sql.Connection;

class ParallelBatchInserts {
  public static void main(String[] args) throws Throwable {
    DataSource ds = Setup.getDataSource();
    Connection conn = ds.getConnection();
    Setup.prepareDBNoIndexes(conn);
    conn.close();
    InsertModes.parallelBatchInserts(10_000, ds);
  }
}