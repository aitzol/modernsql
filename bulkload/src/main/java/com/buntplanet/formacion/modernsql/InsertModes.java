package com.buntplanet.formacion.modernsql;

import javaslang.collection.Iterator;
import javaslang.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

class InsertModes {
  private static final Logger logger = LoggerFactory.getLogger(InsertModes.class);

  static void individualInserts(Connection conn) throws Throwable {
    PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO trips VALUES(?, ?, ?, ?, ?, ?, ?)");

    List<String> lines = Files.lines(getCSVPath()).skip(1).collect(toList());

    time(() -> {
      for (int i = 0; i < lines.size(); i++) {
        feedLine(preparedStatement, lines.get(i));
        preparedStatement.execute();
        if (i % 10000 == 0)
          logger.info("10.000 registros insertados");
      }
    });

    preparedStatement.close();
  }

  static void individualInsertsNoPS(Connection conn) throws Throwable {

    List<String> lines = Files.lines(getCSVPath()).skip(1).collect(toList());

    time(() -> {
      for (int i = 0; i < lines.size(); i++) {
        String[] cols = lines.get(0).split(",");
        conn.createStatement()
            .execute(String.format(
                "INSERT INTO trips VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                cols[0],
                cols[1],
                cols[2],
                cols[3],
                cols[4],
                cols[5],
                cols[6]
            ));
        if (i % 10000 == 0)
          logger.info("10.000 registros insertados");
      }
    });
  }

  static void batchInserts(int batchSize, Connection conn) throws Throwable {
    PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO trips VALUES(?, ?, ?, ?, ?, ?, ?)");

    List<String> lines = Files.lines(getCSVPath()).skip(1).collect(toList());

    time(() -> {
      for (int i = 0; i < lines.size(); i++) {
        feedLine(preparedStatement, lines.get(i));
        preparedStatement.addBatch();
        if (i % batchSize == 0) {
          preparedStatement.executeBatch();
          logger.info("{} registros insertados en batch", batchSize);
        }
      }
      preparedStatement.executeBatch();
      logger.info("Resto de registros () insertados en batch", batchSize % 1000);
    });

    preparedStatement.close();
  }

  public static void copyDisablingIndexes(Connection conn) throws Throwable {
    time(() -> {
      conn.prepareStatement("DROP INDEX trips_duration").execute();
      conn.prepareStatement("DROP INDEX trips_start_time").execute();
      conn.prepareStatement("DROP INDEX trips_end_time").execute();
      logger.info("Índices en tabla 'trips' borrados");

      conn.prepareStatement("" +
          "COPY trips " +
          "FROM '" + getCSVPath().toAbsolutePath().toString() + "'" +
          "WITH (" +
          "FORMAT CSV," +
          "HEADER true" +
          ")").execute();
      logger.info("COPY realizado");

      conn.prepareStatement("CREATE INDEX trips_duration ON trips(duration_ms)").execute();
      conn.prepareStatement("CREATE INDEX trips_start_time ON trips(start_time)").execute();
      conn.prepareStatement("CREATE INDEX trips_end_time ON trips(end_time)").execute();
      logger.info("Índices en tabla 'trips' creados");
    });
  }

  static void copy(Connection conn) throws Throwable {
    time(() -> conn.prepareStatement("" +
        "COPY trips " +
        "FROM '" + getCSVPath().toAbsolutePath().toString() + "'" +
        "WITH (" +
        "FORMAT CSV," +
        "HEADER true" +
        ")").execute());
  }

  static void parallelBatchInserts(int batchSize, DataSource ds) throws Throwable {
    Iterator<javaslang.collection.List<String>> lineBatches = javaslang.collection.List.ofAll(Files.lines(getCSVPath()).skip(1).collect(toList()))
        .sliding(batchSize, batchSize);
    time(() -> lineBatches
        .toJavaList()
        .stream()
        .parallel()
        .forEach(batch -> {
          try {
            Connection conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("INSERT INTO trips VALUES(?, ?, ?, ?, ?, ?, ?)");
            for (String line : batch) {
              feedLine(ps, line);
              ps.addBatch();
            }
            ps.executeBatch();
            logger.info("{} registros insertados en batch", batch.size());
            ps.close();
            conn.close();
          } catch (Throwable e) {
            logger.error("Algo ha fallado...", e);
          }
        }));
  }

  static void parallelMultipleValues(int batchSize, DataSource ds) throws Throwable {
    Iterator<javaslang.collection.List<String>> lineBatches = javaslang.collection.List.ofAll(Files.lines(getCSVPath()).skip(1).collect(toList()))
        .sliding(batchSize, batchSize);
    time(() -> lineBatches
        .toJavaList()
        .stream()
        .parallel()
        .forEach(batch -> {
          try {
            Connection conn = ds.getConnection();
            String sql = "INSERT INTO trips VALUES";
            String values = batch.map(line -> {
              String[] cols = line.replaceAll("'", "_").split(",");
              return String.format(
                  "('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                  cols[0],
                  cols[1],
                  cols[2],
                  cols[3],
                  cols[4],
                  cols[5],
                  cols[6]
              );
            }).mkString(", ");
            conn.createStatement().execute(sql + values);
            logger.info("{} registros insertados en batch", batch.size());
            conn.close();
          } catch (Throwable e) {
            logger.error("Algo ha fallado...", e);
          }
        }));
  }

  private static void feedLine(PreparedStatement preparedStatement, String line) throws SQLException {
    String[] cols = line.split(",");
    preparedStatement.setString(1, cols[0]);
    preparedStatement.setString(2, cols[1]);
    preparedStatement.setString(3, cols[2]);
    preparedStatement.setString(4, cols[3]);
    preparedStatement.setString(5, cols[4]);
    preparedStatement.setString(6, cols[5]);
    preparedStatement.setString(7, cols[6]);
  }

  private static Path getCSVPath() throws URISyntaxException {
    return Paths.get(InsertModes.class.getResource("/2015-Q2-Trips-History-Data.csv").toURI());
  }

  private static void time(Try.CheckedRunnable runnable) throws Throwable {
    LocalDateTime start = LocalDateTime.now();
    runnable.run();
    LocalDateTime end = LocalDateTime.now();
    Duration totalDuration = Duration.between(start, end);
    logger.info("Duration: " + totalDuration.toString());
  }
}
