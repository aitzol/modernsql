* [Temario, curso y recursos](https://docs.google.com/document/d/11N1TFa4vAaDsNqrpLYQnSCleDc3rgbsrOzWsIAtxV7s/edit#)

## Objetivos ##
 
Se trata de un taller que mezcla teoría y práctica y que tiene como objetivo redescubrir el estándar actual de SQL a través de PostgreSQL, una de las bases de datos relacionales open source que soporta un SQL más avanzado y se utiliza para analítica de grandes cantidades de datos. Muchos desarrollos de software se conectan a base datos relacionales. Aunque el estándar SQL-92 ha ido recibiendo múltiples revisiones a lo largo del tiempo (SQL:1999, SQL:2003, SQL:2006, SQL:2008 y SQL:2011), la mayoría de proyectos aún están anclados en el estándar SQL-92. El taller consistirá en una serie de módulos con una charla teórica breve y un ejercicio para realizar operaciones con unos datasets dados. Está basado en el trabajo de divulgación de las bondades del sql de Markus Winand con Modern sql. 

## A quién va dirigido ##
A desarrolladores de software trabajen habitualmente con ORM’s u otras capas de abstracción y que piensen que pueden sacarle más partido a SQL. A desarrolladores que estén pensando en usar bases de datos NoSQL por cuestiones de rendimiento y que sospechen que quizá podrían sacar más de sus bases de datos relacionales. 

## Programa ##

* ¿Por qué...

 * ...moderno?

 - ... postgre?
 - “grandes cantidades datos” (vs big data)?
* Reporting
 * Bulk loading
 * Dimensional modeling
 * Advanced queries
* Modern sql
 * Modern sql
 * Errores habituales
* Se puede hacer en postgre
 * No-sql
 * Full text search
* Otros
 * Postgis
 * Madlib
 * Foreign Data Wrappers


_Feedback:_
_A modo de conclusión final, ¿podrías poner al final del curso algunos ejemplos de sql-moderno vs programación.? Seria similar al copiado, pero con reporting como por ejemplo calcular los totales acumuladas mediante un bucle vs SQL_

## ¿Por qué? ##

* Sql es conocimiento antifrágil
* ¿Postgre like?
** Todos los postgre-like tiene el sql compatible con postgre
*** ¡no necesito HQL!
*** Puedo cambiar un postgre por un MPP como greenplum cuando realmente lo necesite
**** Redshift, citus, etc….

No es
Un taller de optimización de índices y query planner (explain)
Un taller de optimización del postgre.conf
Este curso no es para dba´s ( es para desarrolladores)
Este curso no es para aprender a soportar millones de conexiones web
No es un curso de big data (pero big data DE VERDAD)
Disclaimer
No somos dba´s expertos
No cabe todo el temario ni de coña
¿sql moderno?
..
Grandes cantidades de datos
¿big data?
Work load
Muchas conexiones sql´s sencillas
Muchos datos
Big data
Postgresql
Punto fuerte
reporting/bulk-loading de muchos datos
Transacciones fuertes
Full acid
No usar RDBMS
Cola de mensajes
Almacenar ficheros
Caché de aplicaciones
No usar postgre
Reporting big data
Postgre soporta varios teras de datos (supuestamente por tabla)
Tenemos tablas con 14 millones de filas (1.5 gigas) y sin problemas
MPP ←--- antes de huir a un big data
Mysql 
es más rápido para queries lectura que sean sencillas
Tiene replicado horizontal más sencillo y trillado
En el infinito
Una inmemory database funciona mejor que un postgre en ram
Una no-sql funciona mejor que un postgre con extensiones
Un elastic search escala mejor que un postgre con full-text search
Un neo4j busca más rápido que un WITH RECURSIVE
Datos a nivel “big data”
Diez  tablas de 50 millones de filas de 3.5 gigabytes no es big data
MPP
Ya hadoop, etc

Reporting
Bulk loading 

(cantidad :  csv de un millón de filas ( 100 megas) )
Bucle de inserts individuales
Sobre tabla sin índices / con índices
Jdbc batch insert
Sin/con índices
Jdbc batch insert con threads en paralelo
sin/con índices
Insert multiple values con threads en paralelo
sin/con índnices
Copy
sin/con índices
tirando/creando índices
Mencional pg_bulkload / pg_dump/restore in parallel
Query planner
Explain analyze índices y query planner
Recrear índices puede tardar horas


Dimensional modeling
Base
Fact tables, and foreigh keys
https://en.wikipedia.org/wiki/Fact_table
Star Schema
https://en.wikipedia.org/wiki/Star_schema
Aplanar las relaciones M:N (ejemplo, meters y tags)
http://www.kimballgroup.com/2009/05/the-10-essential-rules-of-dimensional-modeling/

Denormalización (precalculos, duplicidad)
Analítica compleja
¿índices?
Fact: trip
Dimensions: subscription, terminals, dim_day, dim_hour, type
Otros facts: aparcamiento, tiempo medio de viaje
Advanced queries
Nos basamos en:
https://sqlschool.modeanalytics.com/advanced/window-functions/
https://www.compose.io/articles/deeper-into-postgres-9-5-new-group-by-options-for-aggregation/
Ver chuleta.txt!!!

Modern Sql
Errores habituales
https://blog.jooq.org/2013/07/30/10-common-mistakes-java-developers-make-when-writing-sql/
https://blog.jooq.org/2013/08/12/10-more-common-mistakes-java-developers-make-when-writing-sql/
https://blog.jooq.org/2014/05/26/yet-another-10-common-mistakes-java-developer-make-when-writing-sql-you-wont-believe-the-last-one/
Processing data in java memory
No usar tablas temporales (create temp table like other table on commit drop)
Using jdbc pagination
Joining data in java memory
Sorting in java
Not upserting
with upsert as ( update pinned_tweets set (tweet_id, pinned_at) = (3, clock_timestamp()) where user_handle = 'hansolo' returning * ) insert into pinned_tweets (user_handle, tweet_id, pinned_at) select 'hansolo', 3, clock_timestamp() where not exists ( select 1 from upsert where upsert.user_handle = 'hansolo' );

insert into pinned_tweets (user_handle, tweet_id, pinned_at) values ( 'rey', 5, clock_timestamp() ) on conflict (user_handle) do update set (tweet_id, pinned_at) = (5, clock_timestamp()) where pinned_tweets.user_handle = 'rey';
Not using window functions
Inserting lots of records one by one
"COPY trips FROM PROGRAM 'gunzip -c `pwd`/$filename' DELIMITER ',' CSV;"
Not using prepared statements
Returning too many columns
Not using row value expressions (c.first_name, c.last_name) = (?, ?)
Running the same query all the time
N+1 problem <<-- orm´s antipattern
Not using CTE
Recursive cte: naive trees antipattern
WITH RECURSIVE included_parts(sub_part, part) AS ( SELECT sub_part, part FROM parts WHERE part = 'our_product' UNION ALL SELECT p.sub_part, p.part FROM included_parts pr, parts p WHERE p.part = pr.sub_part ) DELETE FROM parts WHERE part IN (SELECT part FROM included_parts);
Row value expressions for updates!!!!
http://modern-sql.com/
SELECT f.* FROM films f, (VALUES('MGM', 'Horror'), ('UA', 'Sci-Fi')) AS t (studio, kind) WHERE f.studio = t.studio AND f.kind = t.kind;
Generate_series <<--- para rellenar los huecos!
FILTER
Antes: SELECT COUNT(*) AS unfiltered, SUM( CASE WHEN i < 5 THEN 1 ELSE 0 END ) AS filtered FROM generate_series(1,10) AS s(i);
Después: SELECT COUNT(*) AS unfiltered, COUNT(*) FILTER (WHERE i < 5) AS filtered FROM generate_series(1,10) AS s(i);


Se puede hacer en postgresql
No-sql Types
Key-value databases, ver recursos hstore (más abajo)
Storing session information
User profiles, preferences
Shopping cart data
Document oriented, ver recursos xml/json support ( más abajo)
Event logging
Content management systems, blogging platforms
Web analytics real-time
E-commerce application
Columnar-family
Event logging
Content management systems, blogging
Counters
Expiring usage
Graph databases
Connected data
Routing, dispatch and location based services
Recommendation engines
Full-text search





Recursos
No-sql
Xml (document oriented)
http://www.postgresql.org/docs/current/static/functions-xml.html
http://www.slideshare.net/EnterpriseDB/the-nosql-way-in-postgres
https://robots.thoughtbot.com/the-durable-document-store-you-didnt-know-you-had-but
http://www.slideshare.net/petereisentraut/postgresql-and-xml
Json (document oriented)
Json Operators: http://schinckel.net/2014/05/25/querying-json-in-postgres/
https://compose.io/articles/could-postgresql-9-5-be-your-next-json-database/
https://vaughnvernon.co/?p=942
http://www.aptuz.com/blog/is-postgres-nosql-database-better-than-mongodb/
https://www.compose.io/articles/is-postgresql-your-next-json-database/
http://blog.codeship.com/unleash-the-power-of-storing-json-in-postgres/
Hstore (key-value)
http://www.postgresql.org/docs/9.4/static/hstore.html
http://stormatics.com/howto-handle-key-value-data-in-postgresql-the-hstore-contrib/
http://postgresguide.com/cool/hstore.html
http://michael.otacoo.com/postgresql-2/postgres-feature-highlight-hstore/
Columnar storage
Citus data (es una extensión, a partir de la última versión)
https://www.citusdata.com/blog/76-postgresql-columnar-store-for-analytics
https://citusdata.github.io/cstore_fdw/

Greenplum (es un fork)
http://gpdb.docs.pivotal.io/4380/admin_guide/ddl/ddl-storage.html#topic39
Graph
http://numergent.com/2015-05/Tropology-performance-PostgreSQL-vs-Neo4j.html


Cómo funciona sql
https://blog.jooq.org/2016/03/17/10-easy-steps-to-a-complete-understanding-of-sql/
https://blog.jooq.org/2013/07/30/10-common-mistakes-java-developers-make-when-writing-sql/
https://blog.jooq.org/2013/08/12/10-more-common-mistakes-java-developers-make-when-writing-sql/
https://blog.jooq.org/2014/05/26/yet-another-10-common-mistakes-java-developer-make-when-writing-sql-you-wont-believe-the-last-one/

Bulk loading
http://www.depesz.com/2007/07/05/how-to-insert-data-to-database-as-fast-as-possible/

Postgis
https://dzone.com/refcardz/essential-postgis
http://tapoueh.org/blog/2013/08/05-earthdistance

Madlib
http://madlib.incubator.apache.org/

CTE
http://postgresguide.com/sexy/ctes.html

Enumerated data types
http://postgresguide.com/sexy/enums.html

Deployment (fork and extensions)
http://greenplum.org/

Redshift 
http://tech.marksblogg.com/billion-nyc-taxi-rides-redshift.html
http://docs.aws.amazon.com/redshift/latest/mgmt/welcome.html 

https://www.citusdata.com/blog/17-ozgun-erdogan/403-citus-unforks-postgresql-goes-open-source
http://tech.marksblogg.com/billion-nyc-taxi-rides-postgresql.html

http://vitessedata.com/



Postgre XL ( mpp)
http://www.postgres-xl.org/


netezza, redshift, greenplum, enterpirse DB, postgre-XL, bigSQL


Dimensional modeling
http://www.kimballgroup.com/wp-content/uploads/2013/08/2013.09-Kimball-Dimensional-Modeling-Techniques11.pdf
http://www.kimballgroup.com/2009/05/the-10-essential-rules-of-dimensional-modeling/


Libros
No-sql distilled
Postgresql up and running
High performance postgresql
Sql antipatterns
http://www.redbook.io/
The Data Warehouse Toolkit: The Complete Guide to Dimensional Modeling


Full-text search
http://rachbelaid.com/postgres-full-text-search-is-good-enough/
https://www.rdegges.com/2013/easy-fuzzy-text-searching-with-postgresql/
http://www.postgresql.org/docs/9.5/static/fuzzystrmatch.html
http://www.postgresql.org/docs/9.1/static/pgtrgm.html
https://devcenter.heroku.com/articles/heroku-postgres-extensions-postgis-full-text-search#full-text-search-dictionaries
https://about.gitlab.com/2016/03/18/fast-search-using-postgresql-trigram-indexes/
https://www.compose.io/articles/indexing-for-full-text-search-in-postgresql/
http://www.sai.msu.su/~megera/wiki/wildspeed 


Foreign Data Wrappers
List: https://wiki.postgresql.org/wiki/Foreign_data_wrappers
From mongodb: http://www.infoq.com/articles/breaking-down-data-silos-with-foreign-data-wrappers

Window functions

Explicación visual de las window functions: https://pbs.twimg.com/media/CTdOf1BXIAA7e99.png

http://askanantha.blogspot.com.es/2007/10/analytic-functions-by-example.html
http://tapoueh.org/tags/window-functions
http://tech.yunojuno.com/sql-window-functions
http://blog.jooq.org/2013/11/03/probably-the-coolest-sql-feature-window-functions/
https://sqlschool.modeanalytics.com/advanced/window-functions/
http://blog.jooq.org/2016/03/24/time-for-some-funky-sql-prefix-sum-calculation/

Sql Performance
Indexes
Query planner
Etc


https://github.com/dhamaniasad/awesome-postgres



Ranking:

http://db-engines.com/en/ranking
http://db-engines.com/en/ranking/relational+dbms
http://db-engines.com/en/ranking_categories
http://db-engines.com/en/ranking_osvsc

Benchmarks:

http://www.tpc.org/information/benchmarks.asp
http://www.tpc.org/tpcc/results/tpcc_results.asp?orderby=dbms
http://www.tpc.org/tpce/results/tpce_results.asp?orderby=dbms
http://www.tpc.org/tpch/results/tpch_results.asp?orderby=dbms
http://www.tpc.org/tpcvms/default.asp
http://blog.pgaddict.com/posts/performance-since-postgresql-7-4-to-9-4-tpc-ds.

MySQL vs Postgres:

https://blog.udemy.com/mysql-vs-postgresql/
https://www.wikivs.com/wiki/MySQL_vs_PostgreSQL
https://www.digitalocean.com/community/tutorials/sqlite-vs-mysql-vs-postgresql-a-comparison-of-relational-database-management-systems
http://www.enterprisedb.com/solutions/mysql-vs-postgresql
http://www.anchor.com.au/hosting/dedicated/mysql_vs_postgres
http://www.slideshare.net/anandology/ten-reasons-to-prefer-postgresql-to-mysql
http://2ndquadrant.com/en/postgresql/postgresql-vs-mysql/
https://www.upguard.com/articles/postgres-vs-mysql
http://dba.stackexchange.com/questions/41232/performance-difference-between-mysql-and-postgresql-for-the-same-schema-queries
https://laracasts.com/discuss/channels/general-discussion/mysql-vs-postgresql

Problemas a la hora de utilizar un NoSQL:

http://sookocheff.com/post/opinion/the-five-stages-of-nosql/

http://daemon.co.za/2014/04/when-is-mongodb-the-right-tool/

http://nosql-database.org/

https://en.wikipedia.org/wiki/NoSQL
http://www.planetcassandra.org/what-is-nosql/
http://searchdatamanagement.techtarget.com/feature/Key-criteria-for-choosing-different-types-of-NoSQL-databases

https://en.wikipedia.org/wiki/CAP_theorem
http://www.infoq.com/articles/cap-twelve-years-later-how-the-rules-have-changed