package com.buntplanet.formacion.modernsql;

import javax.sql.DataSource;
import java.sql.Connection;

class ParallelBatchInsertsIndex {
  public static void main(String[] args) throws Throwable {
    DataSource ds = Setup.getDataSource();
    Connection conn = ds.getConnection();
    Setup.prepareDBNoIndexes(conn);
    Setup.createIndexes(conn);
    conn.close();
    InsertModes.parallelBatchInserts(1000, ds);
  }
}